//
//  HistoryViewController.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import RealmSwift
import TableKit

protocol HistoryViewProtocol: class, BaseView {
    func routesUpdated(routes: Results<OptimalRoute>)
}

class HistoryViewController: UIViewController {
  
  // MARK: - Public properties
  
  lazy var presenter:HistoryPresenterProtocol = HistoryPresenter(view: self)
  
  // MARK: - Private properties
  
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            self.tableDirector = TableDirector.init(tableView: tableView)
            tableView.tableFooterView = UIView()
        }
    }

    var tableDirector: TableDirector!
    // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    presenter.fetchRoutes()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Display logic
  
  // MARK: - Actions
  
  // MARK: - Overrides
    
  // MARK: - Private functions
}

extension HistoryViewController:  HistoryViewProtocol {
    func routesUpdated(routes: Results<OptimalRoute>) {
        
        tableDirector.clear()
        
        let section = TableSection()
        
        for route in routes {
            
            let row = TableRow<RouteCell>.init(item: route.timeStamp?.toString(format: "MMM d, yyyy, HH:mm")).on(.click) { [unowned self] options in
                
                let vc: RouteDetailViewController = UIViewController.loadFromNib()
                vc.route = route
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            section.append(row: row)
        }
        
        tableDirector.append(section: section)
        
        tableDirector.reload()

    }
}
