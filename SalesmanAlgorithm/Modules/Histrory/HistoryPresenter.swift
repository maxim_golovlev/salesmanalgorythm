//
//  HistoryPresenter.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit
import RealmSwift

protocol HistoryPresenterProtocol: class {
    weak var view:HistoryViewProtocol? { get set }
    func fetchRoutes()
}

class HistoryPresenter {
  
  // MARK: - Public variables
  weak var view:HistoryViewProtocol?
  
  // MARK: - Private variables
    
    
    var notificationToken: NotificationToken?
  
  // MARK: - Initialization
  init(view:HistoryViewProtocol) {
    self.view = view
  }
}

extension HistoryPresenter: HistoryPresenterProtocol {
    
    func fetchRoutes() {
        
        let realm = RealmService.shared.realm
        let routes = realm.objects(OptimalRoute.self).sorted(byKeyPath: "timeStamp", ascending: false)

        self.view?.routesUpdated(routes: routes)
        
        notificationToken = realm.observe({ (notification, realm) in
            self.view?.routesUpdated(routes: routes)
        })
        
        RealmService.shared.observeRealmErrors() { (error) in
            self.view?.showAlert(title: "Error", message: error?.localizedDescription)
        }
    }
}
