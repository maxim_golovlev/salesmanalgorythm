//
//  RouteCell.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class RouteCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 44
    }
    
    func configure(with title: String?) {
        self.titleLabel.text = title
    }
    
}
