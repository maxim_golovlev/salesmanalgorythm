//
//  RouteDetailViewController.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import RealmSwift

class RouteDetailViewController: UIViewController {

    @IBOutlet weak var matrixContainer: UIView!
    @IBOutlet weak var shortestPathLabel: UILabel!
    @IBOutlet weak var xContainer: UIView!
    @IBOutlet weak var yContainer: UIView!
    var route: OptimalRoute?
    
    let xStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 2
        return stackView
    }()
    
    let yStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 2
        return stackView
    }()
    
    let matrixStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = 0
        return stackView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        renderXCoordScale(count: route?.matrix?.columns)
        renderYCoordScale(count: route?.matrix?.rows)
        renderMatrix(matrix: route?.matrix)
        renderShortestPath(path: route?.shortestPath)
    }
    
    func renderXCoordScale(count: Int?) {
        
        guard let count = count else {return}
        
        xContainer.addSubview(xStackView)
        xStackView.fillSuperview()
        
        for i in 1...count {
            let label = UILabel()
            label.text = "\(i)"
            label.textAlignment = .center
            label.backgroundColor = .white
            xStackView.addArrangedSubview(label)
        }
    }
    
    func renderYCoordScale(count: Int?) {
        
        guard let count = count else {return}
        
        yContainer.addSubview(yStackView)
        yStackView.fillSuperview()
        
        for i in 1...count {
            let label = UILabel()
            label.text = "\(i)"
            label.textAlignment = .center
            label.backgroundColor = .white
            yStackView.addArrangedSubview(label)
        }
    }
    
    func renderShortestPath(path: List<Int>?) {
        guard let path = path else { return }
        shortestPathLabel.text = path.map({ "\($0)" }).joined(separator: " -> ")
    }
    
    func renderMatrix(matrix: RealmMatrix?) {
        
        guard let matrix = matrix else { return }
        
        matrixContainer.addSubview(matrixStackView)
        matrixStackView.fillSuperview()
        
        for j in 1...matrix.columns {
            
            let rowStackView = UIStackView()
            rowStackView.axis = .horizontal
            rowStackView.distribution = .fillEqually
            rowStackView.spacing = 0
            matrixStackView.addArrangedSubview(rowStackView)
            
            for i in 1...matrix.rows {
                
                let label = UILabel()
                let weight = matrix[j - 1, i - 1]
                label.text = weight == nil ? "M" : "\(weight!)"
                label.textAlignment = .center
                label.layer.borderColor = UIColor.black.cgColor
                label.layer.borderWidth = 1
                
                rowStackView.addArrangedSubview(label)
            }
        }
    }
}
