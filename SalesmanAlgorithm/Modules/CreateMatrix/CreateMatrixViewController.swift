//
//  CreateMatrixViewController.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit

protocol CreateMatrixViewProtocol: class {
  func routeDidLoad(route: OptimalRoute)
}

class CreateMatrixViewController: UIViewController {
  
  // MARK: - Public properties
  
  lazy var presenter:CreateMatrixPresenterProtocol = CreateMatrixPresenter(view: self)
  
  // MARK: - Private properties
  
    @IBOutlet weak var cityCountTextField: UITextField!
    // MARK: - View lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  // MARK: - Display logic
  
  // MARK: - Actions
  
    @IBAction func generateMatrix(_ sender: UIButton) {
        
        if let text = cityCountTextField.text, let size = Int(text), size >= 2, size <= 10 {
            presenter.getMatrix(size: size)
        }
    }
    // MARK: - Overrides
    
  // MARK: - Private functions
}

extension CreateMatrixViewController:  CreateMatrixViewProtocol {
    func routeDidLoad(route: OptimalRoute) {

        let vc:RouteDetailViewController = UIViewController.loadFromNib()
        vc.route = route
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
