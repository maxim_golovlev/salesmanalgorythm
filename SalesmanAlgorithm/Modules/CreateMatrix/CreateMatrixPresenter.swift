//
//  CreateMatrixPresenter.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright (c) 2017 Admin. All rights reserved.
//

import UIKit

protocol CreateMatrixPresenterProtocol: class {
    weak var view:CreateMatrixViewProtocol? { get set }
    func getMatrix(size: Int)
}

class CreateMatrixPresenter {
  
  // MARK: - Public variables
  weak var view:CreateMatrixViewProtocol?
  
  // MARK: - Private variables
  
  // MARK: - Initialization
  init(view:CreateMatrixViewProtocol) {
    self.view = view
  }
}

extension CreateMatrixPresenter: CreateMatrixPresenterProtocol {
    func getMatrix(size: Int) {
        
        let matrix = PathManager.shared.generateMatrix(size: size)
        
        PathManager.shared.calculateOptimalPath(matrix: matrix.copy(), size: size) { pathArray in
            
            guard let pathArray = pathArray else { return }
            
            DispatchQueue.main.async {
                let route = OptimalRoute.init(matrix: matrix, shortestPath: pathArray, timeStamp: Date())
                RealmService.shared.create(route)
                self.view?.routeDidLoad(route: route)
            }
        }
    }
}
