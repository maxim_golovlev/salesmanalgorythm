//
//  PathMatrix.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class PathMatrix: Matrix {
    
    func applyReductionForRows() {
        for row in 1...rows {
            let maxCurrentIndexInRow = row * columns
            let minCurrentIndexInRow = (row - 1) * columns
            if let currentMinInRow = allRowElements(row: row).flatMap ({ $0 }).min() {
                self.grid = self.grid.enumerated().map {
                    if $0.offset < maxCurrentIndexInRow && $0.offset >= minCurrentIndexInRow, let elem = $0.element {
                        return elem - currentMinInRow
                    }
                    return $0.element
                }
            }
        }
    }
    
    func applyReductionForColumns() {
        for column in 1...columns {
            if let currentMinInColumn = allColumnElements(column: column).flatMap ({ $0 }).min() {
                for row in 1...rows {
                    if self[row-1, column-1] != nil {
                        self[row-1, column-1] = self[row-1, column-1]! - currentMinInColumn
                    }
                }
            }
        }
    }
    
    func removeMaxNumber() -> ((Int, Int)?)  {
        var maxNumber: (index:(r:Int, c:Int), value:Int)? = nil
        for r in 1...rows {
            for c in 1...columns {
                if self[r-1, c-1] == 0 {
                    let minInRow = allRowElements(row: r, excluded: (r, c)).flatMap ({ $0 }).min() ?? 0
                    let minInColumn = allColumnElements(column: c, excluded: (r, c)).flatMap ({ $0 }).min() ?? 0
                    let max = minInRow + minInColumn
                    if (maxNumber != nil && max >= maxNumber!.value) || maxNumber == nil  {
                        maxNumber = (index:(r, c), value: max)
                    }
                }
            }
        }
        for r in 1...rows {
            for c in 1...columns {
                if r == maxNumber?.index.r || c == maxNumber?.index.c {
                    self[r-1,c-1] = nil
                }
            }
        }
        
        return ((maxNumber?.index))
    }
    
    func excludeReverseRoute(route: (r: Int, c: Int)?) {
        if let index = route {
            print("exclude road \(index.c, index.r)")
            self[index.c - 1, index.r - 1] = nil
        }
    }
    
    func matrixIndex(arrayIndex index:Int) -> (Int, Int)? {
        
        let originIndex = index
        var row: Int? = nil
        var column: Int? = nil
        
        for i in 0...rows {
            
            let index = originIndex - i * rows
            
            if (index + 1) <= columns {
                row = i + 1
                column = index + 1
                return (row!, column!)
            }
        }
        
        return nil
    }
    
    func checkIfLastRoute() -> (Int, Int)? {
        if self.grid.flatMap ({ $0 }).count == 1 {
            let index = self.grid.enumerated().filter({ $0.element != nil }).map{ $0.offset}.first!
            return matrixIndex(arrayIndex: index)
        }
        return nil
    }
    
    func copy(with zone: NSZone? = nil) -> PathMatrix {
        let copy = PathMatrix.init(rows: rows, columns: columns)
        copy.grid = grid
        return copy
    }
    
}
