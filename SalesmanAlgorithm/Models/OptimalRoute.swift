//
//  OptimalRoute.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class RealmMatrix: Object {
    dynamic var rows: Int = 0, columns: Int = 0
    dynamic var grid = List<Int?>()
    dynamic var id = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(matrix: Matrix) {
        self.init()
        self.rows = matrix.rows
        self.columns = matrix.columns
        self.grid.append(objectsIn: matrix.grid)
    }
    
    func indexIsValid(row: Int, column: Int) -> Bool {
        return row >= 0 && row < rows && column >= 0 && column < columns
    }
    subscript(row: Int, column: Int) -> Int? {
        get {
            assert(indexIsValid(row: row, column: column), "Index out of range")
            return grid[(row * columns) + column]
        }
    }
}

@objcMembers class OptimalRoute: Object {
    
    dynamic var matrix: RealmMatrix?
    dynamic var shortestPath = List<Int>()
    dynamic var timeStamp: Date?
    dynamic var id = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(matrix: PathMatrix, shortestPath: [Int], timeStamp: Date?) {
        self.init()
        
        self.matrix = RealmMatrix.init(matrix: matrix)
        self.shortestPath.append(objectsIn: shortestPath)
        self.timeStamp = timeStamp
        
    }
}
