//
//  Matrix.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 07.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class Matrix {
    let rows: Int, columns: Int
    var grid: [Int?]
    init(rows: Int, columns: Int) {
        self.rows = rows
        self.columns = columns
        grid = Array(repeating: 0, count: rows * columns)
    }
    func indexIsValid(row: Int, column: Int) -> Bool {
        return row >= 0 && row < rows && column >= 0 && column < columns
    }
    subscript(row: Int, column: Int) -> Int? {
        get {
            assert(indexIsValid(row: row, column: column), "Index out of range")
            return grid[(row * columns) + column]
        }
        set {
            assert(indexIsValid(row: row, column: column), "Index out of range")
            grid[(row * columns) + column] = newValue
        }
    }
    
    func allRowElements(row: Int, excluded: (r:Int, c:Int)? = nil) -> [Int?] {
        
        var grid = self.grid
        
        if let obj = excluded {
            grid = self.grid.enumerated().map({ if $0.offset == ((obj.r - 1) * columns) + (obj.c - 1) {return nil} else {return $0.element} })
        }
        return Array(grid[(row - 1)*columns ..< row * columns])
    }
    
    func allColumnElements(column: Int, excluded: (r:Int, c:Int)? = nil) -> [Int?] {
        
        var grid = self.grid
        
        if let obj = excluded {
            grid = self.grid.enumerated().map({ if $0.offset == ((obj.r - 1) * columns) + (obj.c - 1) {return nil} else {return $0.element} })
        }
        
        return Array(1...rows).reduce(into: [], { (result, row) in
            let elementIndex = column + columns * (row - 1)
            let element = grid[elementIndex - 1]
            result.append(element)
        })
    }
}
