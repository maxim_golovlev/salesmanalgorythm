//
//  PathManager.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 07.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class PathManager {
    
    private init() {}
    
    static let shared = PathManager()
    
    var shortestPath = [(Int,Int)]()
    
    func generateMatrix(size: Int) -> PathMatrix {
        
        let matrix = PathMatrix.init(rows: size, columns: size)
        
        let maxIndex = size - 1
        
        for i in 0...maxIndex {
            for j in 0...maxIndex {
                if i == j {
                    matrix[i, j] = nil
                } else {
                    matrix[i, j] = Int(arc4random_uniform(100))
                }
            }
        }
        return matrix
    }
    
    func calculateOptimalPath(matrix: PathMatrix, size: Int, completion: @escaping ([Int]?) -> ()) {
        
        self.shortestPath.removeAll()
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            
            for i in (1...size) {
                let row1 = matrix.allRowElements(row: i)
                print(row1)
            }
            
            for j in 1...size {
                print("new iteration")
                matrix.applyReductionForRows()
                print("red1")
                for i in (1...size) {
                    let row1 = matrix.allRowElements(row: i)
                    print(row1)
                }
                
                matrix.applyReductionForColumns()
                print("red2")
                for i in (1...size) {
                    let row1 = matrix.allRowElements(row: i)
                    print(row1)
                }
                
                let path = matrix.removeMaxNumber()
                print("remove \(j) Path: \(path ?? (0,0))")
                
                if let path = path {
                    self?.shortestPath.append(path)
                }
                
                for i in (1...size) {
                    let row1 = matrix.allRowElements(row: i)
                    print(row1)
                }
                
                if matrix.grid.flatMap({ $0 }).count == 0 {
                    print("that was the last road")
                    break
                }
                
                print("exclude reverse rout")
                matrix.excludeReverseRoute(route: path)
                
                for i in (1...size) {
                    let row1 = matrix.allRowElements(row: i)
                    print(row1)
                }
                
                if let index = matrix.checkIfLastRoute() {
                    print("last route \(index)")
                    
                    self?.shortestPath.append(index)
                    
                    break
                }
                
            }
            
            if let strong = self {
                let sortedPath = strong.sortPathArray(pathArray: strong.shortestPath)
                completion(sortedPath)
                return
            }
            completion([0])
        }
    }
    
    private func sortPathArray(pathArray: [(Int,Int)]) -> [Int] {
        
        if pathArray.count == 1 {
            return pathArray.reduce(into: [Int](), { (res, tuple) in
                res.append(tuple.0)
                res.append(tuple.1)
            })
        }
        
        var arr = [(Int, Int)?]()
        
        arr.append(pathArray.first(where: { $0.0 == 1}))
        
        for _ in 1...(pathArray.count - 1) {
            
            if let last = arr.last {
                arr.append(pathArray.first(where: { last?.1 == $0.0 }))
            }
        }
        
        print(arr)
        
        let filtered = arr.reduce(into: [Int]()) { (res, tuple) in
            
            guard let tuple = tuple else {return}
            
            if let last = res.last, last != tuple.0 {
                res.append(tuple.0)
                res.append(tuple.1)
            } else if res.last == nil {
                res.append(tuple.0)
                res.append(tuple.1)
            } else {
                res.append(tuple.1)
            }
        }
        
        return filtered
    }

}

