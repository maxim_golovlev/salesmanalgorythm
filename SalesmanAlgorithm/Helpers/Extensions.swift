//
//  Extensions.swift
//  SalesmanAlgorithm
//
//  Created by Admin on 20.11.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    func toString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension UIViewController {
    class func loadFromNib<T: UIViewController>() -> T {
        return T(nibName: String(describing: T.self), bundle: nil)
    }
}

